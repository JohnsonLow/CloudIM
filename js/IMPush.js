IMPush = (function(){
	
	var IMPushMsgHandler = null;
	var logFunc = null;
	
	function init(opt){
		logFunc = opt.log || null;
		var info = getPushInfo();
		plus.push.setAutoNotification( false );
		listen(opt.receiveHanlder);
		return info;
	}
	
	function getPushInfo(){
	    var info = plus.push.getClientInfo();
	    
	    outLine( "token: "+info.token );
	    outLine( "clientid: "+info.clientid );
	    outLine( "appid: "+info.appid );
	    outLine( "appkey: "+info.appkey );
	    return info;
	}
	function listen(handler){
		IMPushMsgHandler = handler;
		outLine("listenning");
//		 plus.push.addEventListener( "click", function( msg ) {
//      // 判断是从本地创建还是离线推送的消息
//	        switch( msg.payload ) {
//	            case "LocalMSG":
//	                outLine( "点击本地创建消息启动：" );
//	            break;
//	            default:
//	                outLine( "点击离线推送消息启动：");
//	            break;
//	        }
//	        // 提示点击的内容
//	        plus.ui.alert( msg.content );
//
//	    }, false );
		plus.push.addEventListener( "receive", function( msg ) {
	        if ( msg.aps ) {  // Apple APNS message
	            outLine( "接收到在线APNS消息：" );
	        } else {
	            outLine( "接收到在线透传消息：" );
	        }
	        App.log("payload type is " + typeof msg.payload)
	        IMPushMsgHandler && IMPushMsgHandler(JSON.parse(msg.payload));
	        
	    }, false );
	}
	function outLine(m){
		App.log(JSON.stringify(m));
		logFunc&& logFunc(JSON.stringify(m));
	}
	
	return{
		init: init,	
	}
}())
